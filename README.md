The <a href="https://www.doditsolutions.com/redbus-clone/">redbus clonne script</a> is an online bus ticket booking and reservation software, built on PHP and MySQL with Code Igniter framework that allows you to manage your API/OWN Bus Inventory, fares, routes, schedules etc. Book the tickets of any bus as in one-way or round-trip with secure transactions.
<ul><li>
UNIQUE FEATURES:

Multilingual
Special Discounts
Customization
Timely Departure and Arrival
Efficiency and Availability
Updated Events Info
Return Tickets
Career Link
Seat Layout
Currency Converter
Routes Information
Weather Forecasting</li></ul>

<ul><li>
USER PANEL:

⦁ Customer login credentials along with forgot password option.

⦁ Personal account details

⦁ Email notification

⦁ Search option enabled for source city and destination city

⦁ Check on available bus details, select desired seat, and boarding point

⦁ Send the booked ticket details via SMS

⦁ Send the booked ticket details via Email

⦁ Can view en-numbers of bus snaps and videos

⦁ User Wallet available

⦁ Payment Gateway enabled

⦁ Print tickets

⦁ Check refund status by entering the ticket number</li></ul>
<ul><li>
BUS ADMIN PANEL:

⦁ Login credentials

⦁ Admin Profile

⦁ Agent details to view

⦁ Block/unblock agent record

⦁ Password change

⦁ Bus details to be managed ( add/delete/edit/block/unblock/pagination)

⦁ Seat structure designed

⦁ Bus images and videos managed

⦁ Passenger details managed

⦁ Ticket details managed

⦁ Seat details managed

⦁ Ticket bookings managed

⦁ Cancellation managed

⦁ Payment details managed

⦁ View bus transaction details

⦁ Cancelling policies

⦁ SMS details managed

⦁ Email details managed</li></ul>
<ul><li>
AGENT PANEL:

⦁ Login credentials

⦁ Control Panel managed

⦁ Agent Profile

⦁ Deposits

⦁ Target and Incentives managed

⦁ Booking reports managed

⦁ Ledger details managed

⦁ Monthly-yearly-today chart managed

⦁ Bus transaction reports managed

⦁ Wallet/print ticket/cancellation managed</li></ul>

<ul><li>

ADMIN PANEL:

⦁ Login credentials

⦁ Tickets sold (day,month,year)

⦁ Ticket sale graph

⦁ City management

⦁ Manage the city details (edit, status, delete, pagination)

⦁ Route management

⦁ User management

⦁ Manage the users details(edit,delete,status)

⦁ Passengers management

⦁ Ticket details

⦁ Booker details

⦁ Seat details

⦁ Bus type management

⦁ Ticket booking managed

⦁ Ticket cancellation

⦁ Seat managed

⦁ Payment managed

⦁ Commission managed

⦁ Refund status

⦁ Cancelling Policy

⦁ Bus service details

⦁ Advantage of ticket booking

⦁ Bulk SMS managed

⦁ Sms log details

⦁ Email log details

⦁ Banners managed

⦁ Manage marquee text

⦁ Day-to-day offer coupon codes would be generated.

Watch this space for updates.</li></ul>



